FROM node:10.15.0-stretch-slim as build-phase
ARG env=staging
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build:${env}

FROM nginx:latest
ARG env=staging
COPY nginx.${env}.conf /etc/nginx/conf.d/default.conf
COPY --from=build-phase /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]